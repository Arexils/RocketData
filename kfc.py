import json

import requests


def pretty_output(data) -> list:
    """
    Parse and output pretty data to list
    @type data: restaurant['storePublic']['openingHours']['regularDaily'] get json data
    """
    time_start = data['startTimeLocal'][:-3]
    time_end = data['endTimeLocal'][:-3]
    weekend = f'сб-вс {time_start}-{time_end}'
    workday = f'пн - пт {time_start} до {time_end}'
    return [workday,
            weekend,
            ]


def parse_kfc():
    """
        try exept использую для отработки исключений(на случай если на сайте поменяется структура), чтоб валидные данных записались
        При работе проблем ошибок не возникает
    """
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
        'Accept': 'application/json, text/plain, */*',
    }

    params = {
        'showClosed': 'true',
    }

    restaurants_list = requests.get('https://api.kfc.com/api/store/v2/store.get_restaurants', params=params, headers=headers).json()["searchResults"]

    with open('output/kfc.json', 'w', encoding='utf-8') as outfile:
        list_temp_rest = []
        for restaurant in restaurants_list:
            try:
                if restaurant['storePublic']['title']['ru'].lower() in 'testтест':
                    """Как заметил в тестовых данных которые выдает api там есть такие такие результаты 'Текст по умолчанию', 'Тест 2' и тд.
                    Таким образом я "пытаюсь" отсечь данные которые отправляются как тестовые для api
                    """
                    continue
                data = {"address": restaurant['storePublic']['contacts']['streetAddress']['ru'],
                        "latlon": restaurant['storePublic']['contacts']['coordinates']['geometry']['coordinates'],
                        "name": restaurant['storePublic']['title']['ru'],
                        "phones": restaurant['storePublic']['contacts']['phoneNumber'],
                        "working_hours": pretty_output(restaurant['storePublic']['openingHours']['regular']) if restaurant['storePublic']['status'] == 'Open' else ['Closed', ],
                        }
                list_temp_rest.append(data)
            except Exception as e:
                print(f'{e}\n {restaurant}')
        json.dump(list_temp_rest, outfile, ensure_ascii=False)

    print('Successful parse kfc')


if __name__ == '__main__':
    parse_kfc()
