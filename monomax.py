import json
import re

import requests


def parse_monomax():
    """
    try exept использую для отработки исключений(на случай если на сайте поменяется структура), чтоб валидные данных записались
    При работе проблем ошибок не возникает
    """
    r = requests.get('https://monomax.by/map')

    pattern = r'(ymaps\.Placemark(?:\(|\s+|.+)+?\))'
    card_info = re.findall(pattern, r.text)

    with open('output/monomax.json', 'w', encoding='utf-8') as outfile:
        list_temp_monomax = []
        for card in card_info:
            try:
                temp = list(map(lambda x: re.sub("'", '', x), re.findall("'.+'", card)))
                data = {"address": temp[0],
                        "latlon": re.findall('\d+\.\d+', card),
                        "name": 'Мономах',
                        "phones": [re.sub('Телефон:', '', temp[1])],
                        }
                list_temp_monomax.append(data)
            except Exception as e:
                print(f'{e}\n {card}')

        json.dump(list_temp_monomax, outfile, ensure_ascii=False)
    print('Finished parse monomax')


if __name__ == '__main__':
    parse_monomax()
