import json
import re

import requests
from lxml import html


def parse_ziko():
    """
        try exept использую для отработки исключений(на случай если на сайте поменяется структура), чтоб валидные данных записались
        При работе проблем ошибок не возникает
    """
    xpath_phone = '//tbody/tr[contains(@class,"{id_card}")]/td[@class="mp-table-address"]/text()'

    response_front = requests.get('https://www.ziko.pl/lokalizator/').text
    response = requests.get('https://www.ziko.pl/wp-admin/admin-ajax.php?action=get_pharmacies').json()

    with open('output/ziko.json', 'w', encoding='utf-8') as outfile:
        temp_list_ziko = []
        page = html.fromstring(response_front)
        for card in response.values():
            try:
                get_card_tel = [re.sub('\s+\w+\..', '', tel) for tel in page.xpath(xpath_phone.format(id_card=card['mypostid'])) if 'tel' in tel]
                data = {"address": card['address'],
                        "latlon": [card['lat'], card['lng']],
                        "name": card['title'],
                        "phones": get_card_tel,
                        "working_hours": re.findall('[-\s\w]+\d\d:\d\d.-.\d\d:\d\d', re.sub('<br>', ' ', card['mp_pharmacy_hours']).replace('niedziela handlowa', 'nie'))[:3],
                        }
                temp_list_ziko.append(data)
            except Exception as e:
                print(f'{e}\n {card}')

        json.dump(temp_list_ziko, outfile, ensure_ascii=False)
    print('Finished parse ziko')


if __name__ == '__main__':
    parse_ziko()
